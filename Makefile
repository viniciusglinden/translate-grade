# Vinícius Gabriel Linden
# 2020-11-03

PROG = translate-grades
CXX = g++ -std=c++11
INCLUDE= src
CXXFLAGS = -Wall -I $(INCLUDE)
LDFLAGS = -lncurses
OBJS = *.cpp

$(PROG):
	$(CXX) $(LDFLAGS) ${CXXFLAGS} -o $(PROG) $(INCLUDE)/$(OBJS)

debug: clean
	$(CXX) $(LDFLAGS) ${CXXFLAGS} -o $(PROG) $(INCLUDE)/$(OBJS)

run: debug
	@echo "----------------------------------------------------"
	./$(PROG)

clean:
	rm -f $(PROG)

.PHONY: clean debug run test
