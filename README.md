
# Translate Grades

A basic C++ application for translating grades between different systems.

## Supported note types

- Brazil, base 10
- Brazil, base 100
- Canada, base 100
- Germany, RWTH
<!--- Greece, UTC-->

## References

- [Tabelle zur Umrechnung von Noten für im Ausland erbrachte Studien- und
Prüfungsleistungen](https://www.rwth-aachen.de/global/show_document.asp?id=aaaaaaaaaamlewj)
- [FitMyCurve](https://mycurvefit.com/)
