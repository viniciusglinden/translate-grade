/**
 * @file grade.hpp
 *
 * @author Vinícius Gabriel Linden
 *
 * @date 2020-11-02
 *
 * @brief See README.md
 *
 */

#ifndef GRADE_H_
#define GRADE_H_

#include <iostream>

class grade {
private:
	float fl_base_grade { 0 }; //< default: Brazilian 100
	bool b_initialized = false; //< Initialized flag, print will display error if not set

	inline void error_not_initialized();
public:
	grade();

	enum grade_systems {
		uninitialized,
		brazil_10,
		brazil_100,
		canada_100,
		germany
	};

	/* SETTING */

	bool is_initialized();
	int set(grade_systems input_type, float grade);

	int set_brazil_10(float grade);
	int set_brazil_100(float grade);
	int set_canada_100(float grade);
	int set_germany(float grade);

	/* PRINTING */

	void print(grade_systems output_type);

	void print_brazil_10();
	void print_brazil_100();
	void print_canada_100();
	void print_germany();

};

#endif /* ifndef GRADE_H_ */
