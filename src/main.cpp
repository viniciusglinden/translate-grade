/**
 * @file translate-grades.cpp
 *
 * @author Vinícius Gabriel Linden
 *
 * @date 2020-11-02
 *
 * @brief See README.md
 *
 */

#include "grade.hpp"
#include <cstring>
#include <iostream>
#include <string>
#include <ncurses.h>
#include <vector>

inline void help()
{
	std::cout <<
		"Usage, command line mode:" << std::endl <<
		"     translate-grades str_input_type str_output_type GRADE" << std::endl <<
		"  _TYPE OUT_TYPE GRADE" << std::endl <<
		"Usage, interactive mode:" << std::endl <<
		"     translate-grades" << std::endl <<
		"" << std::endl <<
		"AVAILABLE TYPES" << std::endl <<
		"  brazil_10: Brazilian, type 10" << std::endl <<
		"  brazil_100: Brazilian, type 100" << std::endl <<
		"  canada_100: Canadian, type 100" << std::endl <<
		"  germany: German, RWTH" << std::endl
		;
}

int main(int argc, char *argv[])
{

	grade grade;

	if(argc != 1) /* command-line mode */
	{

		if(!strcmp(argv[1],"--help")) {
			help();
			return 0;
		}

		if(argc != 4) {
			std::cout << "Insufficient arguments" << std::endl;
			return 0;
		}

		const std::string str_input_type = argv[1];
		const std::string str_output_type = argv[2];
		const std::string std_input_grade = argv[3];

		float fl_input_grade = std::stof(std_input_grade);

		/* TODO: improve this, either by using a pointer to a function, or using
		 * the grade.set() and grade.print() functions */
		if (str_input_type == "brazil_10") grade.set_brazil_10(fl_input_grade);
		else if(str_input_type == "brazil_100") grade.set_brazil_100(fl_input_grade);
		else if(str_input_type == "canada_100") grade.set_canada_100(fl_input_grade);
		else if(str_input_type == "germany") grade.set_germany(fl_input_grade);
		else {
			std::cout << "Invalid input grade type" << std::endl;
			return 0;
		}

		if (str_output_type == "brazil_10") grade.print_brazil_10();
		else if(str_output_type == "brazil_100") grade.print_brazil_100();
		else if(str_output_type == "canada_100") grade.print_canada_100();
		else if(str_output_type == "germany") grade.print_germany();
		else {
			std::cout << "Invalid output grade type" << std::endl;
			return 0;
		}

		return 0;
	}

	/* interactive mode */

	//initscr();
	//WINDOW *input_win = newwin(LINES-1,COLS/2,1,0);
	//WINDOW *output_win = newwin(LINES-1,COLS/2,1,COLS/2+1);
	//refresh();
	//box(input_win,0,0);
	//box(output_win,0,0);
	//mvaddstr(0,0,"Enter note");
	//wrefresh(input_win);
	//mvaddstr(0,COLS/2+1,"Exit note");
	//wrefresh(output_win);

	//std::vector<std::string> vec_input_grade(1);
	//std::vector<std::string> vec_output_grade(1);

	//vec_input_grade.push_back("");
	//mvwaddstr(input_win,1,1,vec_input_grade[0].c_str());
	//mvwaddstr(input_win,1,1,vec_input_grade.size());
	//wrefresh(input_win);

	//getch();
	//endwin();
	
	//std::vector<std::string> teste(1);
	//std::string tmp;
	//while (teste.size() < 10) {
		//for(std::string i : teste)
			//std::cout << i << ", ";
		//std::cout << "\nTamanho: " << teste.size() << std::endl;
		//std::cin >> tmp;
		//if(teste[0] == "")
			//teste[0] = tmp;
		//else
			//teste.push_back(tmp);
	//}
	//std::cout << tmp << std::endl;

	return 0;
}

