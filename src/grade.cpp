/**
 * @file grade.cpp
 *
 * @author Vinícius Gabriel Linden
 *
 * @date 2020-11-19
 *
 * @brief See README.md
 *
 */

#include "grade.hpp"

inline void grade::error_not_initialized() {
	std::cout << "No grade has been set yet" << std::endl;
	exit(0);
}

grade::grade()
{
	/* user doesn't know what the default type is */
	fl_base_grade = -1.00f;
	b_initialized = false;
};

bool grade::is_initialized() {
	return b_initialized;
}

int grade::set(grade_systems input_type, float grade)
{
	int return_value = 0;
	switch(input_type) {
		case brazil_10:
			return_value = set_brazil_10(grade);
		case brazil_100:
			return_value = set_brazil_100(grade);
		case canada_100:
			return_value = set_canada_100(grade);
		case germany:
			return_value = set_germany(grade);
		default:
			std::cout << "Error: grade.set" << std::endl;
			exit(0);
	}
	return return_value;
}

int grade::set_brazil_10(float grade)
{
	b_initialized = true;
	if (grade > 10 || grade < 0)
		return -1;
	fl_base_grade = 10 * grade;
	return 0;
}

int grade::set_brazil_100(float grade)
{
	b_initialized = true;
	if (grade > 100 || grade < 0)
		return -1;
	fl_base_grade = grade;
	return 0;
}

int grade::set_canada_100(float grade)
{
	return grade::set_brazil_100(grade);
}

int grade::set_germany(float grade)
{
	b_initialized = true;
	if (grade > 5 || grade < 0)
		return -1;
	fl_base_grade = 109.8377f -14.93506f * grade;
	return 0;
}

void grade::print(grade_systems output_type)
{
	switch(output_type) {
		case brazil_10:
			print_brazil_10();
		case brazil_100:
			print_brazil_100();
		case canada_100:
			print_canada_100();
		case germany:
			print_germany();
		default:
			std::cout << "Error: grade.print" << std::endl;
			exit(0);
	}
}

void grade::print_brazil_10()
{
	if (!b_initialized) error_not_initialized();
	std::cout << fl_base_grade / 10 << std::endl;
}

void grade::print_brazil_100()
{
	if (!b_initialized) error_not_initialized();
	std::cout << fl_base_grade << std::endl;
}

void grade::print_canada_100()
{
	if (!b_initialized) error_not_initialized();
	print_brazil_100();
}

void grade::print_germany()
{
	if (!b_initialized) error_not_initialized();
	std::cout << 7.350909f -0.06690909f*fl_base_grade << std::endl;
}
